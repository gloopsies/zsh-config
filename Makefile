MANAGERS=$(shell ls managers/*)

all:
	cp .zshrc.template .zshrc
	@./pm_chooser.sh
	@PM=$$(cat pm.txt); \
	echo $$PM; \
	if [ ! -z "$$PM" ] && (echo $(MANAGERS) | grep "$$PM") > /dev/null; then \
		echo "" >> .zshrc; \
		cat "managers/$$PM" >> .zshrc; \
	else \
		echo "NO"; \
	fi
	rm -f pm.txt

root: all
	@sed 's/%ROOT//g' .zshrc > .zshrc.tmp
	mv .zshrc.tmp .zshrc
	
doas: all
	@sed 's/%ROOT/doas/g' .zshrc > .zshrc.tmp
	mv .zshrc.tmp .zshrc
	
sudo: all
	@sed 's/%ROOT/sudo/g' .zshrc > .zshrc.tmp
	mv .zshrc.tmp .zshrc
		
install: .zshrc
	cp .zshrc ~/.zshrc
	mkdir -p ~/config/zsh
	touch ~/.config/zsh/histfile

clean:
	rm -f .zshrc
