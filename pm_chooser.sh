#!/bin/sh

managers=$(ls managers/)

for pm in $managers; do
	if command -v $pm >/dev/null 2>&1; then
		PM=$pm
		break 1
	fi
done

if [ -z "$PM" ]
then
	echo "No supported package manager found."
else
	echo "Found $PM as your package manager. Do you want to change selection?"
	read -p "Enter your package manager [$PM]: " pm
	if [ ! -z "$pm" ]
	then
		if [ "$pm" = "none" ] || [ "$pm" = "n" ]
		then
			PM=""
		elif (echo $managers | grep "$pm") > /dev/null
		then
			PM=$pm
		else
			echo "There is no config for $pm, Using $PM"
		fi
	fi
fi

echo -n $PM > pm.txt
